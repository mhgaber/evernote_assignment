package com.abodeltae.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by nazeer on 9/15/15.
 */
public class DrawerFragment extends Fragment {
    private View parenView;
    ImageView closeDrawer;
    DrawerInterFace drawerInterFace;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parenView=inflater.inflate(R.layout.drawer_fragment,null);
        initComponents();
        return parenView;
    }

    private void initComponents() {
        closeDrawer= (ImageView) findViewbyId(R.id.imageViewCloseDrawer);
        closeDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerInterFace.closeDrawer();
            }
        });

    }

    private View findViewbyId(int id ){
        return parenView.findViewById(id);
    }
    public void setDrawerInterFace(DrawerInterFace drawerInterFace){
        this.drawerInterFace = drawerInterFace;
    }


}
