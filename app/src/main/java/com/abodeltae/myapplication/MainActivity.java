package com.abodeltae.myapplication;

import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    DrawerLayout drawerLayout;
    RelativeLayout drawerContainer,bottomBar;
    ImageView drawerToggle;
    DrawerInterFace interFace;
    HorizontalScrollView bottomScrollView;
    LinearLayout bottomEditorContainer;
    ImageView editorToolsToggle , scrollIndicator,imageViewBulletFormat,textForamtBold,tags;
    Typeface helvaticaTypeFace;
    EditText noteTitle_edittext,noteBody_edittext;
    TextView noteType_textview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        helvaticaTypeFace=Typeface.createFromAsset(getApplicationContext().getAssets(),
                String.format(Locale.US, "fonts/%s", "HelveticaNeue.ttf"));
        initComponents();
    }

    private void initComponents() {
        drawerLayout= (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerContainer=(RelativeLayout)findViewById(R.id.drawerContainer);
        drawerToggle=(ImageView)findViewById(R.id.imageViewOpenDrawer);
        drawerToggle.setOnClickListener(this);
        scrollIndicator=(ImageView)findViewById(R.id.imageViewScrollIndicator);
        editorToolsToggle=(ImageView)findViewById(R.id.imageViewTextEditorToggle);
        imageViewBulletFormat=(ImageView)findViewById(R.id.imageViewBulletFormat);
        tags=(ImageView)findViewById(R.id.imageViewTags);
        tags.setOnClickListener(this);
        textForamtBold=(ImageView)findViewById(R.id.textformatBold);
        editorToolsToggle.setOnClickListener(this);
        bottomEditorContainer=(LinearLayout)findViewById(R.id.bottomItemsContainerLayout);
        bottomScrollView=(HorizontalScrollView)findViewById(R.id.scrollView);
        bottomBar=(RelativeLayout)findViewById(R.id.bottomBar);
        noteTitle_edittext=(EditText)findViewById(R.id.notetitle_edittext);
        noteTitle_edittext.setTypeface(helvaticaTypeFace);
        noteType_textview= (TextView) findViewById(R.id.notetype);
        noteType_textview.setTypeface(helvaticaTypeFace);
        noteBody_edittext= (EditText) findViewById(R.id.notebody_edittext);
        noteBody_edittext.setTypeface(helvaticaTypeFace);
        handleBottomBar();
        interFace=new DrawerInterFace() {
            @Override
            public void closeDrawer() {
                drawerLayout.closeDrawers();
            }
        };
        DrawerFragment fragment=new DrawerFragment();
        fragment.setDrawerInterFace(interFace);
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.drawerContainer,fragment).commit();
    }

    private void handleBottomBar() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenwidth = size.x;
        float scale = getResources().getDisplayMetrics().density;
        int fourtydpAsPixels = (int) (40*scale + 0.5f);
        findViewById(R.id.bottomBar).measure(size.x, size.y);
        int botombarWidth=bottomEditorContainer.getMeasuredWidth();
        if(screenwidth>=botombarWidth){
            scrollIndicator.setVisibility(View.INVISIBLE);

            bottomScrollView.setPadding(0,0,0,0);
        }
        else {


            bottomScrollView.setPadding(0, 0, fourtydpAsPixels, 0);
            scrollIndicator.setVisibility(View.VISIBLE);
        }

        bottomScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {

                HandleScroll();



            }
        });
    }


    @Override
    public void onClick(View view) {
        int id =view.getId();
        switch (id){
            case R.id.imageViewOpenDrawer:
                drawerLayout.openDrawer(Gravity.RIGHT);
                break;
            case R.id.imageViewTextEditorToggle:
                editorToolsToggle.setImageResource(bottomBar.getVisibility()==View.VISIBLE?R.drawable.ic_text_format_gray:R.drawable.ic_text_format_green);
                bottomBar.setVisibility(bottomBar.getVisibility() == View.VISIBLE ? View.GONE : view.VISIBLE);
                break;
            case R.id.imageViewTags:
                    TagsFragment fragment=new TagsFragment();
                fragment.show(getSupportFragmentManager().beginTransaction(),null);
                break;
        }
    }
    private void HandleScroll() {
        Rect scrollBounds = new Rect();
        bottomScrollView.getDrawingRect(scrollBounds);

        float lastITemleft = imageViewBulletFormat.getX();
        float lastItemRight = lastITemleft + imageViewBulletFormat.getWidth();
        float firstItemleft=textForamtBold.getX();


        if ( scrollBounds.right >= lastItemRight) {
            scrollIndicator.setImageResource(R.drawable.scroll_right);

        }
        else if (scrollBounds.left<=firstItemleft){
            scrollIndicator.setImageResource(R.drawable.scroll_left);}

    }



}
