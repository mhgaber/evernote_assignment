package com.abodeltae.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.dpizarro.autolabel.library.AutoLabelUI;

/**
 * Created by nazeer on 9/16/15.
 */
public class TagsFragment extends DialogFragment {
    View parentView;
    AutoLabelUI autoLabelUI;
    ListView listView ;
    ArrayAdapter<String> adapter;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        TextView textView=new TextView(getContext());
        textView.setText("Choose Tags");
        textView.setTextAppearance(getContext(), android.R.style.TextAppearance_Large);
        textView.setPadding(40, 40, 40, 40);
        textView.setTextColor(getResources().getColor(R.color.app_green));
        parentView=LayoutInflater.from(getContext()).inflate(R.layout.tags_fargment, null);
        initComponents();
        builder.setView(parentView);
        builder.setCustomTitle(textView);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        return builder.create();
    }



    private void initComponents() {
        listView= (ListView) findViewById(R.id.listView);
        autoLabelUI= (AutoLabelUI) findViewById(R.id.label_view);
        adapter=new ArrayAdapter<String>(getContext(),0){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
               Vh vh;
                if(convertView==null){
                    convertView=LayoutInflater.from(getContext()).inflate(R.layout.tag_row,null);
                    vh=new Vh();
                    vh.checkBox= (CheckBox) convertView.findViewById(R.id.checkBox);
                    vh.textView=(TextView)convertView.findViewById(R.id.textView9);
                    vh.view=convertView;
                    vh.pos=position;
                    convertView.setTag(vh);
                }
                else{
                    vh=(Vh)convertView.getTag();
                }
                vh.textView.setText(getItem(position));
                vh.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos=((Vh)view.getTag()).pos;
                        CheckBox checkBox= (CheckBox) view.findViewById(R.id.checkBox);
                        if(checkBox.isChecked()){
                            checkBox.setChecked(false);
                            autoLabelUI.removeLabel(adapter.getItem(pos));

                        } else{
                            checkBox.setChecked(true);
                            autoLabelUI.addLabel(adapter.getItem(pos));

                        }


                    }
                });
                return convertView;
            };
            class Vh{
                View view;
                TextView textView;
                CheckBox checkBox;
                int pos;
            }
        };
        adapter.add("action");
        adapter.add("aproved");
        adapter.add("home");
        adapter.add("IFTTT");
        adapter.add("rejected");
        adapter.add("ShopMAte");

    listView.setAdapter(adapter);
    }

    private View findViewById(int id){
        return parentView.findViewById(id);
    }
}
